package controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Scanner;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;


	public Controller() {
		view = new MovingViolationsManagerView();
		
		movingViolationsQueue = new Queue<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>();
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				//					this.loadMovingViolations();
				break;

			case 2:
				IQueue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
				view.printDailyStatistics(dailyStatistics);
				break;

			case 3:
				view.printMensage("Ingrese el número de infracciones a buscar");
				int n = sc.nextInt();

				IStack<VOMovingViolations> violations = this.nLastAccidents(n);
				view.printMovingViolations(violations);
				break;

			case 4:	
				fin=true;
				sc.close();
				break;
			}
		}
	}


	/**
	 * 
	 */
	public void loadMovingViolations(){

		String pth;

		for(int j=0; j< 2; j++){

			if(j==0)
				pth = "./data/Moving_Violations_Issued_in_January_2018_ordered.csv";
			else
				pth = "./data/Moving_Violations_Issued_in_February_2018_ordered.csv";

			Path myPath = Paths.get(pth);
			try (BufferedReader br = Files.newBufferedReader(myPath)){


				HeaderColumnNameMappingStrategy<VOMovingViolations> strategy = new HeaderColumnNameMappingStrategy<>();
				strategy.setType(VOMovingViolations.class);
				CsvToBean csvToB = new CsvToBeanBuilder(br).withType(VOMovingViolations.class).withMappingStrategy(strategy).withIgnoreLeadingWhiteSpace(true).build();


				for(Iterator<VOMovingViolations> i = csvToB.parse().iterator();i.hasNext(); ){

					VOMovingViolations at = i.next();
					movingViolationsQueue.enqueue(at);
					movingViolationsStack.push(at);
				}

			}

			catch (FileNotFoundException e1) {

			} 
			catch (IOException e) {

			}			
		}

	}




	/**
	 * 
	 * @return
	 */
	public IQueue <VODaylyStatistic> getDailyStatistics () {

		IQueue<VODaylyStatistic> rta = new Queue<VODaylyStatistic>();

		VOMovingViolations act = null;
		Iterator<VOMovingViolations> iter = movingViolationsQueue.iterator();
		Queue<VOMovingViolations> local = new Queue<VOMovingViolations>();

		while(iter.hasNext())
		{
			VOMovingViolations nxt = iter.next();

			if(act != null)
			{

				if(act.getTicketIssueDate().substring(0, 10).equals(nxt.getTicketIssueDate().substring(0,10)))
				{
					local.enqueue(act);
					act = nxt;
				}
				else{
					local.enqueue(act);
					VODaylyStatistic h = new VODaylyStatistic(act.getTicketIssueDate().substring(0,10),local);
					rta.enqueue(h);
					local = new Queue<VOMovingViolations>();
					act = nxt;

				}

			}

			else
				act = nxt;
		}

		if(!iter.hasNext()){		
			local.enqueue(act);
			VODaylyStatistic h = new VODaylyStatistic(act.getTicketIssueDate().substring(0,10),local);
			rta.enqueue(h);
		}


		return rta;
	}


	public IStack <VOMovingViolations> nLastAccidents(int n) {
		
		IStack<VOMovingViolations> ultimas = new Stack<VOMovingViolations>();
		Iterator<VOMovingViolations> iter = movingViolationsStack.iterator();
		if(n<=movingViolationsStack.size()){
				while(n>0)
				{
					
					if(iter.hasNext() && iter.next().getAccidentIndicator().equals(true))
					{
						VOMovingViolations actual= iter.next();
						ultimas.push(iter.next());
						System.out.println(""+ actual.getobjectid() + " " + actual.getTicketIssueDate() + " " + actual.getLocation() + " " + actual.getViolationDesc() + "/n");
						n--;
					}
					
				}
		}
		return ultimas;
	}
}
