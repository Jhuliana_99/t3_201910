package model.data_structures;

import java.util.Iterator;
import linked.list.LinkedList;

public class Queue<T> implements IQueue<T>{

	/**
	 * Tamaño actual de la lista
	 */
	private int size;

	/**
	 * Lista encadenada 
	 */
	private LinkedList<T> linkedlist;



	//Constructor

	/**
	 * Construye una nueva cola vacia.
	 */
	public Queue()
	{
		size = -1;
		linkedlist = new LinkedList<T>();
	}


	/**
	 * Retorna el iterador de la cola
	 * @return retornó el iterador de la cola
	 */
	public Iterator<T> iterator() {
		return linkedlist.iterator();
	}

	/**
	 * Indica si la cola está vacia o llena
	 * @return true si la lista está vacia, false en caso contrario
	 */
	public boolean isEmpty() {
		if(size==-1)
			return true;
		else
			return false;
	}

	/**
	 * Retorna el tamaño de la lista, comenzando desde 1.
	 * @return un entero con el tamaño de la lista.
	 */
	public int size() {

		return size + 1;
	}

	/**
	 * Añade un nuevo elemento al final de la lista.
	 */
	public void enqueue(T t)
	{
		linkedlist.addAtEnd(t);
		size++;
	}

	/**
	 * Saca el primer elemento de la lista, eliminandolo de la estructura.
	 * @return retorna el elemento eliminado
	 */
	public T dequeue() {

		if(!isEmpty()){
			T h =linkedlist.deleteAtK(0);
			size --;
			return h;
		}
		else
			return null;
	}


}
