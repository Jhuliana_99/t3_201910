package model.data_structures;

import java.util.Iterator;

import linked.list.LinkedList;

public class Stack<T> implements IStack<T> {

	//Atributos
	/**
	 * Modela el tama�o de la pila.
	 */
	private int tama�o;
	
	/**
	 * Modela la lista de elementos de la pila.
	 */
	private LinkedList<T> lista;
	
	//Constructores
	
	/**
	 * Inicializa el tama�o en 0 y la lista encadenada que almacena los elementos de la pila.
	 */
	public Stack()
	{
		tama�o = 0;
		lista = new LinkedList<T>();
	}
	
	//Metodos
	
	/**
	 * Retorna el iterador de la pila.
	 */
	@Override
	public Iterator<T> iterator() {
		return lista.iterator();
	}

	/**
	 * Retorna true si la pila esta vacia y false si la pila tiene por lo menos un elemento.
	 */
	@Override
	public boolean isEmpty() {
		if(tama�o == 0)
		{
			return true;
		}
		
		return false;
	}

	
	/**
	 * Retorna el tama�o de la lista.
	 */
	@Override
	public int size() {
		return tama�o;
	}

	
	/**
	 * A�ade el elemento al comienzo de la pila.
	 */
	@Override
	public void push(T t) {
		lista.addAtK(0, t);
		tama�o++;
	}

	/**
	 * Retorna el primer elemento de la pila y lo elimina de la pila.
	 */
	@Override
	public T pop() {
		T elemento = lista.getElement(0);
		lista.delete(elemento);
		return elemento;
	}

}
