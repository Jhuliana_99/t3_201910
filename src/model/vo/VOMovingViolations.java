package model.vo;

import com.opencsv.bean.CsvBindByName;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {



	@CsvBindByName
	private String row_;

	@CsvBindByName(column = "OBJECTID")
	private int objectid;

	@CsvBindByName
	private String location;

	@CsvBindByName(column = "ADDRESS_ID")
	private String address_id;

	@CsvBindByName(column = "STREETSEGID")
	private String streetsegid;

	@CsvBindByName(column = "XCOORD")
	private String xcoord;

	@CsvBindByName(column = "YCOORD")
	private String ycoord;

	@CsvBindByName(column = "TICKETTYPE")
	private String tickettype;

	@CsvBindByName(column = "FINEAMT")
	private int fineamt;

	@CsvBindByName(column = "TOTALPAID")
	private int totalpaid;

	@CsvBindByName(column = "PENALTY1")
	private String penalty1;

	@CsvBindByName(column = "PENALTY2")
	private String penalty2;

	@CsvBindByName(column = "ACCIDENTINDICATOR")
	private String accidentindicator;

	@CsvBindByName(column = "TICKETISSUEDATE")
	private String ticketissuedate;

	@CsvBindByName(column = "VIOLATIONCODE")
	private String violationcode;

	@CsvBindByName(column = "VIOLATIONDESC")
	private String violationdesc;

	@CsvBindByName(column = "ROW_ID")
	private String row_id;

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int getobjectid() {
		return objectid;
	}

	/**
	 * No sirve para nada pero es necesario para que openscv pueda construir el objeto :))
	 */
	public String getRow_() {
		return row_;
	}

	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return address_id - retorna la ID de la direcci�n.
	 */
	public String getAddress_id()
	{
		return address_id;
	}

	/**
	 * @return streetsegeid - retorna la ID del segmento de la calle.
	 */
	public String getStreetsegid()
	{
		return streetsegid;
	}

	/**
	 * @return xcoord - retorna la coordenada x donde sucedi� la infracci�n.
	 */
	public String getXcoord()
	{
		return xcoord;
	}

	/**
	 * @return ycoord - retorna la coordenada y donde sucedi� la infracci�n.
	 */
	public String getYcoord()
	{
		return ycoord;
	}

	/**
	 * @return tickettype.
	 */
	public String getTickettype()
	{
		return tickettype;
	}

	/**
	 * @return fineamt - retorna la cantidad a pagar por la infracci�n en USD.
	 */
	public int getFineamt()
	{
		return fineamt;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public int getTotalPaid() {
		return totalpaid;
	}

	/**
	 * @return penalty1 - retorna el dinero extra que debe pagar el conductor.
	 */
	public String getPenalty1()
	{
		return penalty1;
	}

	/**
	 * @return penalty2 - retorna el dinero extra que debe pagar el conductor.
	 */
	public String getPenalty2()
	{
		return penalty2;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentindicator;
	}

	/**
	 * @return date - retorna la fecha cuando se puso la infracci�n .
	 */
	public String getTicketIssueDate() {
		return ticketissuedate;
	}

	/**
	 * @return violationcode - retorna el c�digo de la infracci�n.
	 */
	public String  getViolationcode() {
		return violationcode;
	}

	/**
	 * @return row_id
	 */
	public String getrow_id()
	{
		return row_id;
	}

	/**
	 * @return violatuondesc - Descripci�n textual de la infracci�n.
	 */
	public String  getViolationDesc() {
		return violationdesc;
	}


	/**
	 * Le da un valor al identificador �nico de la infracci�n.
	 * @param objectId, Identificador �nico de la infracci�n.
	 */
	public void setobjectid(int objectId) {
		objectid = objectId;
	}	

	/**
	 * Le da un valor a row_
	 * @param r
	 */
	public void setRow_(String r) {
		row_ = r;
	}

	/**
	 * Le da un valor a la ID de la direcci�n.
	 * @param ad, ID de la direcci�n.
	 */
	public void setAddress_id(String ad)
	{
		address_id = ad;
	}


	/**
	 * Le da un valor a la direcci�n.
	 * @param loc, direcci�n.
	 */
	public void setLocation(String loc) {
		location = loc;
	}

	/**
	 * Le da un valor a la ID del segmento de la calle.
	 * @param ssd, ID del segmento de la calle.
	 */
	public void setStreetsegid( String ssd)
	{
		streetsegid = ssd;
	}

	/**
	 * Le da un valor a la coordenada x .
	 * @param x, coordenada x.
	 */
	public void setXcoord(String x)
	{
		xcoord = x;
	}

	/**
	 * Le da un valor a la coordenada y.
	 * @param y, coordenada y.
	 */
	public void setYcoord(String y)
	{
		ycoord = y;
	}

	/**
	 */
	public void setTickettype(String tt)
	{
		tickettype = tt;
	}

	/**
	 * Le da un valor a la cantidad a pagar por la infracci�n en USD.
	 * @param fa, la cantidad a pagar por la infracci�n en USD.
	 */
	public void setFineamt( int fa)
	{
		fineamt = fa;
	}


	/**
	 * Le da un valor al dinero pagado por la persona que realiz� la infracci�n.
	 * @param tp, dinero pagado por la persona que realiz� la infracci�n.
	 */
	public void setTotalPaid(int tp) {
		totalpaid= tp;
	}

	/**
	 * Le da un valor al dinero extra que tiene pagar el conductor.
	 * @param p1, dinero extra que tiene pagar el conductor.
	 */
	public void getPenalty1(String p1)
	{
		penalty1= p1;
	}

	/**
	 * Le da un valor al dinero extra que tiene pagar el conductor.
	 * @param p2, dinero extra que tiene pagar el conductor.
	 */
	public void setPenalty2(String p2)
	{
		penalty2= p2;
	}

	/**
	 * Le da un valor a la fecha cuando se puso la infracci�n .
	 * @param tid - Fecha cuando se puso la infracci�n .
	 */
	public void setTicketIssueDate(String tid) {
		ticketissuedate = tid;
	}


	/**
	 * Le da un valor al indicador de accidentes
	 * @rparam ai - Si hubo un accidente o no.
	 */
	public void  setAccidentIndicator(String ai) {
		accidentindicator = ai;
	}

	/**
	 * Le da un valor a la Descripci�n textual de la infracci�n.
	 * vd, descripci�n textual de la infracci�n..
	 */
	public void  setViolationDes(String vd) {
		violationdesc = vd;
	}

	/**
	 * 
	 * @param ri
	 */
	public void setRow_id(String ri)
	{
		row_id = ri;
	}


	/**
	 * Retorna una cadena de texto con algunos datos de la infracci�n
	 */
	public String toString() {

		return objectid + row_  + " | "+ ticketissuedate + " | " + location + " | " + violationdesc + "\n";
	}
}
