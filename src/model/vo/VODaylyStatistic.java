package model.vo;

import java.util.Iterator;

import model.data_structures.Queue;

public class VODaylyStatistic {

	/**
	 * D�a de las estad�sticas
	 */
	private String day;

	/**
	 * N�mero de accidentes
	 */
	private int numAcc;

	/**
	 * N�mero de infracciones
	 */
	private int numInfr;

	/**
	 * Total de fineamt
	 */
	private int fineamtT;


	/**
	 * Crea un nuevo DaylyStatistics, inicializa todos los datos.
	 * @param cola, es la cola que contiene todos las infracciones de un d�a espec�fico
	 */
	public VODaylyStatistic(String dd, Queue<VOMovingViolations> cola)
	{
		day = dd;
		numAcc = 0;
		numInfr = 0;
		fineamtT = 0;
		leerCola(cola);
	}

	/**
	 * Lee la informaci�n de la cola y toma los datos estad�sticos necesarios
	 * @param cola, es la cola que contiene todos las infracciones de un d�a espec�fico
	 */
	public void leerCola(Queue<VOMovingViolations> cola)
	{
		Iterator<VOMovingViolations> iter = cola.iterator();

		while(iter.hasNext())
		{
			VOMovingViolations f = iter.next();

			numInfr++;

			fineamtT += f.getFineamt();

			if(f.getAccidentIndicator().equalsIgnoreCase("Yes"))
				numAcc++;


		}
	}


	/**
	 * Devuelve una cadena con las estad�sticas
	 */
	public String toString()
	{
		return "* "+ day + " =  [ \n" + "   N�mero de accidentes:  " + Integer.toString(numAcc) + ", \n" + "   N�mero de infracciones:  " + Integer.toString(numInfr) + ", \n" + "   Multas totales:  $" + Integer.toString(fineamtT) + " .  ] \n";

	}
}
