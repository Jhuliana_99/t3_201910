package model.data_structures;

import java.util.Iterator;

import org.junit.Before;

import junit.framework.TestCase;

public class testStack<T> extends TestCase{
	
	/**
	 * Clase a testear.
	 */
	private Stack stack;
	
	/**
	 * Se instancia la situacion a probar.
	 */
	@Before
	public void setupscenario1()
	{
		stack = new Stack();
	}
	
	/**
	 * Prueba del iterador.
	 */
	public void testIterator()
	{
		try{
			setupscenario1();
			Iterator iterador = stack.iterator();
			// Primer caso
			assertFalse("No deberia haber elementos", iterador.hasNext());
			
			//Segundo caso
			T t = (T) new Object();
			stack.push(t);
			Iterator iterator = stack.iterator();
			assertTrue("Deberia haber un elemento",iterator .hasNext());
		}catch(Exception e)
		{
			fail("No deberia generar exception");
		}
	}
	
	/**
	 * Prueba del metodo que indica si hay o no elementos en el stack.
	 */
	public void testIsEmpty()
	{
		try
		{
			setupscenario1();
			
			//Primer Caso
			assertTrue("Deberia estar vacia",stack.isEmpty());
			
			//Segundo Caso
			T t = (T) new Object();
			stack.push(t);
			assertFalse("Deberia haber elementos", stack.isEmpty());
		}catch(Exception e)
		{
			fail("No deberia generar Excepcion");
		}
	}
	
	/**
	 * Prueba el metodo que rotorna la longitud del Stack.
	 */
	public void testSize()
	{
		try
		{
			setupscenario1();
			
			//Primer Caso
			assertEquals("Deberia estar vacia",0,stack.size());
			
			//Segundo Caso
			T t = (T) new Object();
			stack.push(t);
			stack.push(t);
			stack.push(t);
			assertEquals("Deberia haber elementos",3, stack.size());
		}catch(Exception e)
		{
			fail("No deberia generar Excepcion");
		}
	}
	
	/**
	 * Prueba el metodo push
	 */
	public void testPush()
	{
		try
		{
			setupscenario1();
			
			//Primer Caso
			
			T t = (T) new Object();
			stack.push(t);
			assertTrue("EL elemento se debio crear", !stack.isEmpty());
			
		}catch(Exception e)
		{
			fail("No deberia generar Excepcion");
		}
	}
	
	/**
	 * Prueba que el metodo pop elimine el elemento y ademas lo retorne correctamente
	 */
	public void testPop()
	{
		try
		{
			setupscenario1();
			
			//Primer Caso
			
			T t = (T) new Object();
			T t1 = (T) new Object();
			stack.push(t);
			stack.pop();
			assertFalse("EL elemento se debio eliminar", stack.isEmpty());
			
			//Segundo caso
			
			stack.push(t);
			stack.push(t1);
			assertEquals("Deberia ser el elemento correcto",t1,stack.pop());
			
		}catch(Exception e)
		{
			fail("No deberia generar Excepcion");
		}
	}
	
	

}
